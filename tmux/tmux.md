tmux/tmate

tmux is a multiplexer terminal
tmate multiplexer terminal.

key is : C-b

| key+n     | *n*ext                  |
| key+p     | *p*revious              |
| key+"     | split pane horizontal   |
| key+%     | split pane vertical     |
| key+,     | rename window           |
| key+c     | *c*reate a window       |
| key+z     | toggle *z*oom           |
| key+[     | Enter exploration buffer |
| key+Pgup  | idem |
| q         | Exit exploration buffer |

